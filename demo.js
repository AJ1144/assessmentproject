$(document).ready(function () {
  // Retrieve the template data from the HTML (jQuery is used here).
  var template = $("#demo").html();
  // Compile the template data into a function
  var templateScript = Handlebars.compile(template);
  // Define data in JSON format.
  var context = {
    name: "Alekhya",
  };
  // Pass Data to template script.
  var html = templateScript(context);
  // Insert the HTML code into the page
  $(document.body).append(html);
});
